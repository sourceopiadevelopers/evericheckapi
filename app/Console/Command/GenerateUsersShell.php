<?php

App::uses('User', 'Model');

/**
 * 
 */
class GenerateUsersShell extends AppShell
{

    public $username;
    public $password;

    /**
     * Creates an Admin User : prompts for username and password
     * 
     * @return void();
     */
    public function main()
    {
        $this->username = $this->in("Please Enter a Name");
        $this->password = $this->in("Please Enter a password");

        $this->User = new User();
        $this->__generateUser();
    }

    

    /**
     * After a record has been saved in approval table user must be created in 
     * users table:: therefore creates a record in users table
     * 
     * @return void
     */
    private function __generateUser()
    {
        $this->User->create();
        $user = array(
            'username' => $this->username,
            'password' => $this->password,
            'active' => 'yes'
        );
        $this->User->save($user);
    }

 }
