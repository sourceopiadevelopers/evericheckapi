<?php

/**
 * VERICHECK INC CONFIDENTIAL
 * 
 * Vericheck Incorporated 
 * All Rights Reserved. * 
 * NOTICE: 
 * All information contained herein is, and remains the property of 
 * Vericheck Inc, if any.  The intellectual and technical concepts 
 * contained herein are proprietary to Vericheck Inc and may be covered 
 * by U.S. and Foreign Patents, patents in process, and are protected 
 * by trade secret or copyright law. Dissemination of this information 
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Vericheck Inc.
 *
 * @copyright VeriCheck, Inc. 
 * @version $$1:Gateway Portal $$
 */
App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController
{

    public $uses = array('ResponseClass');

    /**
     * Default function executed before firt statement in the controller.
     */
    public function beforeFilter()
    {
        $this->RequestHandler->ext = 'json';
        parent::beforeFilter();
        $this->Auth->allow();
    }

    /**
     * Fetch Users from users table 
     * 
     * @return array containing Users
     */
    public function index()
    {
        if ($this->request->is('get'))
        {
            $this->_viewData = $this->User->fetch();
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Insert a record in Users table 
     * 
     * return string
     *
      public function create()
      {
      if ($this->request->is('post'))
      {
     */
    public function create()
    {
        if ($this->request->is('post'))
        {
            $this->request->data = array('User' => $this->request->input('json_decode', true));
            $this->_viewData = $this->User->add($this->request->data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows POST requests.');
        }
    }

    /**
     * Login to BOOK API
     * 
     * @return array containing Session Id
     */
    public function login()
    {
        if ($this->request->is('post'))
        {
            $this->request->data = $this->request->input('json_decode', true);
            $user = $this->User->getLoginCredential($this->request->data);
            if (!empty($user))
            {
                $this->loginAction($user['User']);
            } else
            {
                $this->_viewData = $this->setHttpError(401);
            }
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows POST requests.');
        }
    }

    /**
     * Check If User is Valid or not for Authentication
     * 
     * @param type $user users credential
     */
    public function loginAction($user)
    {
        if ($this->Auth->login($user))
        {
            $this->_viewData = $this->ResponseClass->successLogin($user);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('Invalid username or password, try again');
        }
    }

    /**
     * Logout Functionality
     */
    public function logout()
    {
        $this->Auth->logout();
        $this->_viewData = $this->ResponseClass->successLogout('Logout Successful');
    }

}
