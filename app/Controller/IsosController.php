<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

class IsosController extends AppController
{

    public $uses = array('ResponseClass');

    /**
     * 
     * Default function executed before first statement in the controller.
     */
    public function beforeFilter()
    {
        $this->RequestHandler->ext = 'json';
        parent::beforeFilter();
        $this->Auth->allow();
    }

    /**
     * 
     * Fetch iso merchant activity 
     * @return array containing monthly iso activity
     */
    public function fetchMonthlyActivity()
    {

        if ($this->request->is('get'))
        {
            $params = array();

            if (!empty($this->request->query))
            {
                $params = $this->request->query;
                $params['iso'] = $this->request->params['isoId'];
                $this->_viewData = $this->Iso->fetchMonthlyIsoActivity($params);
            } else
            {
                $this->_viewData = $this->ResponseClass->error('Start date and End date required');
            }
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch all isos  
     * @return array containing isos
     */
    public function fetch()
    {

        if ($this->request->is('get'))
        {
            $this->_viewData = $this->Iso->getIsos();
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch all isos on the basis of id
     * @return array containing isos by id
     */
    public function fetchById()
    {

        if ($this->request->is('get'))
        {
            $params = array(
                'iso' => $this->request->params['id']
            );
            $this->_viewData = $this->Iso->fetchIsosById($params);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch all isos current month activity
     * @return array containing all iso's current month activity
     */
    public function fetchCurrentMonthActivity()
    {

        if ($this->request->is('get'))
        {
            $this->_viewData = $this->Iso->fetchIsoCurrentMonthlyActivity();
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch current iso merchants activity 
     * @return array containing Users
     */
    public function fetchMerchantsCurrentMonthActivity()
    {

        if ($this->request->is('get'))
        {
            $isoId = $this->request->params['isoId'];
            $param = array('iso' => $isoId);
            $this->_viewData = $this->Iso->fetchIsoMerchantsCurrentActivity($param);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

}
