<?php

App::uses('AppController', 'Controller');

class MerchantsController extends AppController
{

    public $uses = array('ResponseClass');

    /**
     * 
     * Default function executed before firt statement in the controller.
     */
    public function beforeFilter()
    {
        $this->RequestHandler->ext = 'json';
        parent::beforeFilter();
        $this->Auth->allow();
    }

    /**
     * 
     * Fetch all merchants
     * @return array containing all merchants
     */
    public function fetch()
    {

        if ($this->request->is('get'))
        {
            $this->_viewData = $this->Merchant->fetchMerchants();
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch all merchants by id 
     * @return array containing merchants
     */
    public function fetchById()
    {

        if ($this->request->is('get'))
        {
            $params = array();

            if (!empty($this->request->query))
            {
                $params = $this->request->query;
            }

            $params['id'] = $this->request->params['id'];
            $this->_viewData = $this->Merchant->fetchMerchantsByParams($params);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch merchant monthly activity
     * @return array containing merchant monthly activity
     */
    public function fetchMonthlyActivity()
    {

        if ($this->request->is('get'))
        {
            $params = array();

            if (!empty($this->request->query))
            {
                $params = $this->request->query;
                $params['merchant'] = $this->request->params['mid'];
                $this->_viewData = $this->Merchant->fetchMonthlyMerchantActivity($params);
            } else
            {
                $this->_viewData = $this->ResponseClass->error('Start date and End date required');
            }
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch Merchant Current Month Activity
     * 
     * @return array containing monthly activity of all merchants
     */
    public function fetchCurrentMonthActivity()
    {

        if ($this->request->is('get'))
        {
            $params = array();
            
            if (!empty($this->request->query))
            {
               $params = $this->request->query;  
               $this->_viewData = $this->Merchant->fetchMerchantCurrentMonthlyActivity($params);
           }
            
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

}
