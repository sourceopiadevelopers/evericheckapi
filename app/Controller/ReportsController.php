<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

class ReportsController extends AppController
{

    public $uses = array('ResponseClass');

    /**
     * Default function executed before firt statement in the controller.
     */
    public function beforeFilter()
    {
        $this->RequestHandler->ext = 'json';
        parent::beforeFilter();
        $this->Auth->allow();
    }

    /**
     * Fetch Bathes from batches table 
     * 
     * @return array containing Users
     */
    public function getBatches()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchBatches($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch isos from iso table 
     * 
     * @return array containing isos
     */
    public function getIsos()
    {
        if ($this->request->is('get'))
        {
            $this->_viewData = $this->Report->fetchIsos();
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch iso by Id from iso  table 
     * 
     * @return array containing isos
     */
    public function getIsosById()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchIsosById($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch merchants from merchant table 
     * 
     * @return array containing merchants
     */
    public function getMerchants()
    {
        if ($this->request->is('get'))
        {
            $this->_viewData = $this->Report->fetchMerchants();
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch merchant by Id from merchant  table 
     * 
     * @return array containing merchants
     */
    public function getMerchantsById()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchMerchantsById($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch merchant by isoId from merchant  table 
     * 
     * @return array containing merchants
     */
    public function getMerchantsByIsoId()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchMerchantsByIsoId($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch monthly merchant activity 
     * 
     * @return array containing merchant activity
     */
    public function getMonthlyMerchantActivity()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchMonthlyMerchantActivity($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch iso merchant activity 
     * 
     * @return array containing iso activity
     */
    public function getMonthlyIsoActivity()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchMonthlyIsoActivity($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch current iso merchants activity 
     * 
     * @return array containing Users
     */
    public function getIsoMerchantsCurrentMonthActivity()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchIsoMerchantsCurrentActivity($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * Fetch current iso monthly activity 
     * 
     * @return array containing Users
     */
    public function getIsosCurrentMonthActivity()
    {
        if ($this->request->is('get'))
        {
            $this->_viewData = $this->Report->fetchIsoCurrentMonthlyActivity();
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * This resources contains the high level monthly transaction summaries
     * for all merchants
     * for current and last month.
     */
    public function getMerchantCurrentMonthlyActivity()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Report->fetchMerchantCurrentMonthlyActivity($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

}
