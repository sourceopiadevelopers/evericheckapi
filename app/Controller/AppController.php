<?php

/**
 * VERICHECK INC CONFIDENTIAL
 * 
 * Vericheck Incorporated 
 * All Rights Reserved.
 * 
 * NOTICE: 
 * All information contained herein is, and remainsa the property of 
 * Vericheck Inc, if any.  The intellectual and technical concepts 
 * contained herein are proprietary to Vericheck Inc and may be covered 
 * by U.S. and Foreign Patents, patents in process, and are protected 
 * by trade secret or copyright law. Dissemination of this information 
 * or reproduction of this material is strictly forbidden unless prior 
 * written permission is obtained from Vericheck Inc.
 *
 * @copyright VeriCheck, Inc. 
 * @version $$Id: AppModel.php 1694 2013-09-26 09:26:01Z anit $$
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *    
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $uses = array('ResponseClass');
    public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            ),
            'authenticate' => array('JwtToken')
        ),
        'Session',
        'RequestHandler');

    /**
     * Data to be loaded in the view
     * 
     * @var Array
     */
    protected $_viewData;

    /**
     * Default function executed before firt statement
     * in the controller.
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->RequestHandler->ext = 'json';
    }

    /**
     * Default function called after the view is rendered.
     * 
     */
    public function afterFilter()
    {
        parent::afterFilter();
    }

    /**
     * Default function called before view is rendered.
     */
    public function beforeRender()
    {
        if (get_class($this) == 'CakeErrorController')
        {
            $this->_viewData = $this->setHttpError($this->response->statusCode());
        }
        $this->set(array(
            __CLASS__ => $this->_viewData,
            '_serialize' => __CLASS__));
    }

    /**
     * Generic function to load view
     */
    protected function _setResponse($authfail = false)
    {
        if ($authfail == true)
        {
            echo json_encode($this->_viewData);
        }
        $this->set(array(
            __CLASS__ => $this->_viewData,
            '_serialize' => __CLASS__));
    }

    /**
     * Set error message
     * 
     * @param int $httpResponseCode 404, 401 etc
     * return array array('success' => 'false',
     * 											'message'=> String)
     */
    public function setHttpError($httpResponseCode)
    {
        $errorMessage = "";
        if ($httpResponseCode == 404)
        {
            $errorMessage = 'Requested URL Not Found';
            http_response_code(404);
        }
        if ($httpResponseCode == 401)
        {
            $errorMessage = 'Invalid Authentication';
            http_response_code(401);
        }

        return $this->ResponseClass->error($errorMessage);
    }

}
