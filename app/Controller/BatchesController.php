<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppController', 'Controller');

class BatchesController extends AppController
{

    public $uses = array('ResponseClass');

    /**
     * Default function executed before firt statement in the controller.
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow();
        $this->RequestHandler->ext = 'json';
    }

    /**
     * 
     * Fetch Bathes from batches table 
     * @return array containing all batches
     */
    public function fetch()
    {
        if ($this->request->is('get'))
        {
            $data = $this->request->query;
            $this->_viewData = $this->Batch->fetchBatches($data);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

    /**
     * 
     * Fetch all batches by ids (batchid, batchId and merchantId, batchId and IsoId)
     * @return array containing batches
     */
    public function fetchById()
    {

        if ($this->request->is('get'))
        {
            $param = array();

            if (!empty($this->request->query))
            {
                $param = $this->request->query;
                $param['id'] = $this->request->params['id'];
            } else
            {
                $param = array('id' => $this->request->params['id']);
            }

            $this->_viewData = $this->Batch->fetchBatches($param);
        } else
        {
            $this->_viewData = $this->ResponseClass->error('This resource only allows GET requests.');
        }
    }

}
