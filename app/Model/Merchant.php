<?php

App::uses('AppModel', 'Model');
App::uses('RequestClass', 'Model');
App::uses('Calculation', 'Model');

class Merchant extends AppModel
{

    /**
     * 
     * Default Constructor for merchant model
     */
    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->RequestClass = new RequestClass();
        $this->Calculation = new Calculation();
    }

    /**
     * 
     * Fetch all merchants
     * @return type array containing merchants
     */
    public function fetchMerchants()
    {
        $response = $this->RequestClass->getMerchants();

        if (!isset($response['success']))
        {
            $response = array('success' => TRUE, 'payload' => $response);
        }

        return $response;
    }

    /**
     * 
     * Fetch Merchants By Params
     * @return type array containing merchants 
     */
    public function fetchMerchantsByParams($data)
    {
        $param = $this->Calculation->getParams($data);
        $response = $this->RequestClass->getMerchantsByParams($param);


        if (!isset($response['success']))
        {
            $response = array('success' => TRUE, 'payload' => $response);
        }

        return $response;
    }

    /**
     * 
     * Fetch Monthly Merchant Activity of Merchant
     * @return type array containing merchant monthly activity
     */
    public function fetchMonthlyMerchantActivity($data)
    {
        $param = $this->Calculation->getParams($data);
        $merchantRaw = $this->RequestClass->getMonthlyMerchantActivity($param);

        if (!isset($merchantRaw['success']))
        {
            if (!empty($merchantRaw))
            {
                $merchantRaw = $this->__generateMerchantMonthlyActivity($merchantRaw);
            }

            $merchantRaw = array('success' => TRUE, 'payload' => $merchantRaw);
        }

        return $merchantRaw;
    }
    
    private function __generateMerchantMonthlyActivity($merchantData)
    {
        $monthlyActivity = array();
        foreach ($merchantData as $key => $activity)
        {
            $monthlyActivity[$key]['last_txn_mtd_count'] = ($activity[0]['last_mtd_txn_count'] !=null ? $activity[0]['last_mtd_txn_count']:0);
            $monthlyActivity[$key]['mtd_txn_count'] = ($activity[0]['mtd_txn_count'] !=null ? $activity[0]['mtd_txn_count']:0);
            $monthlyActivity[$key]['mtd_txn_count_variance'] = $this->Calculation->calculateVariance($activity[0]['mtd_txn_count'], $activity[0]['last_mtd_txn_count']);
            
            $monthlyActivity[$key]['last_mtd_txn_sum'] = ($activity[0]['last_mtd_txn_sum'] !=null ? $activity[0]['last_mtd_txn_sum']:0);
            $monthlyActivity[$key]['mtd_txn_sum'] = ($activity[0]['mtd_txn_sum'] != null ? $activity[0]['mtd_txn_sum']:0);
            $monthlyActivity[$key]['mtd_txn_sum_variance'] = $this->Calculation->calculateVariance($activity[0]['mtd_txn_sum'], $activity[0]['last_mtd_txn_sum']);
        
            $monthlyActivity[$key]['last_month_txn_count'] = ($activity[0]['last_month_txn_count'] != null ? $activity[0]['last_month_txn_count']:0);
            $monthlyActivity[$key]['txn_count'] = ($activity[0]['txn_count'] != null ? $activity[0]['txn_count'] : 0);
            $monthlyActivity[$key]['txn_count_variance'] = $this->Calculation->calculateVariance($activity[0]['txn_count'], $activity[0]['last_month_txn_count']);
        
            $monthlyActivity[$key]['last_month_txn_sum'] = ($activity[0]['last_mtd_txn_sum'] !=null ? $activity[0]['last_mtd_txn_sum']:0);
            $monthlyActivity[$key]['txn_sum'] = ($activity[0]['txn_sum'] != null ? $activity[0]['txn_sum']:0);
            $monthlyActivity[$key]['txn_sum_variance'] = $this->Calculation->calculateVariance($activity[0]['txn_sum'], $activity[0]['last_mtd_txn_sum']);
        
        }
        
        return $monthlyActivity;
    }
 
    /**
     * 
     * Fetch Current Month Activity of all Merchants
     * @return type array containing current merchnat activity of all merchants
     */
    public function fetchMerchantCurrentMonthlyActivity($page)
    {
        $pageNumber = $this->Calculation->getParams($page);
        $startDate = date('Y-m');
        $records = array();
        $merchantCurrentDayToSent = array();
        $merchants = $this->RequestClass->getMerchantsByPage($pageNumber, 1);

        if (!empty($merchants) && !isset($merchants['success']))
        {
            $merchantKeys = array_keys($merchants);
            $merchantList = implode(',', $merchantKeys);
            $merchantCurrentDayToSent = array('start_date' => $startDate, 'merchant' => $merchantList);
            $param = $this->Calculation->getParams($merchantCurrentDayToSent);
            $activities = $this->RequestClass->getMonthlyMerchantActivity($param);
            $merchantYearYoCurrentDayToSent = array('start_date' => date('Y-01'),'end_date' => date('Y-m'), 'merchant' => $merchantList);
            $yearparam = $this->Calculation->getParams($merchantYearYoCurrentDayToSent);
            $yearlyActivities = $this->RequestClass->getMonthlyMerchantActivity($yearparam); 
           
            if (!isset($activities['success']))
            {
                $records = $this->__generateActivity($merchants, $activities[$startDate], $yearlyActivities);
            }
        }
        
        return array('success' => TRUE, 'payload' => $records);
    }

    /**
     * 
     * Generate current month activity for merchants one by one
     * @return type array containing monthly activity of all merchants
     */
    private function __generateActivity($merchants, $activities, $yearlyActivities)
    {
        $monthlyActivity = array();
        $ytdRecords = $this->__calculateYTD($yearlyActivities);
        
        foreach ($activities as $activity)
        {
            $newkey = $activity['merchantId'];
            if(strlen($newkey) == 2) {
                $key = '00'.$newkey;
            }
            if(strlen($newkey) == 3) {
                $key = '0'.$newkey;
            } 
            if(strlen($newkey) > 3) {
                $key = $newkey;
            }
            
            $monthlyActivity[$key]['merchant_id'] = $key;
            $monthlyActivity[$key]['merchant_name'] = $merchants[$key]['name'];
            $monthlyActivity[$key]['last_txn_mtd_count'] = ($activity['last_mtd_txn_count'] != null ? $activity['last_mtd_txn_count']:0);
            $monthlyActivity[$key]['current_txn_mtd_count'] = ($activity['mtd_txn_count'] != null ? $activity['mtd_txn_count']:0);
            
            $monthlyActivity[$key]['number_of_transaction_variance'] = $this->Calculation->calculateVariance($activity['mtd_txn_count'], $activity['last_mtd_txn_count']);
            $monthlyActivity[$key]['number_of_transaction_gain'] = $this->Calculation->calculateGain($activity['mtd_txn_count'], $activity['last_mtd_txn_count']);
            $monthlyActivity[$key]['number_of_transaction_difference'] = $this->Calculation->calculateDifference($activity['mtd_txn_count'], $activity['last_mtd_txn_count']);
            
            $monthlyActivity[$key]['last_mtd_txn_sum'] = ($activity['last_mtd_txn_sum'] != null ? $activity['last_mtd_txn_sum'] : 0);
            $monthlyActivity[$key]['current_mtd_txn_sum'] = ($activity['mtd_txn_sum'] !=null ? $activity['mtd_txn_sum'] : 0);
            
            $monthlyActivity[$key]['sum_of_transaction_variance'] = $this->Calculation->calculateVariance($monthlyActivity[$key]['current_mtd_txn_sum'], $monthlyActivity[$key]['last_mtd_txn_sum']);
            $monthlyActivity[$key]['sum_of_transaction_gain'] = $this->Calculation->calculateGain($monthlyActivity[$key]['last_mtd_txn_sum'], $activity['last_mtd_txn_sum']);
            $monthlyActivity[$key]['sum_of_transaction_difference'] = $this->Calculation->calculateDifference($activity['mtd_txn_sum'], $activity['last_mtd_txn_sum']);
        
            $monthlyActivity[$key]['number_of_transaction_ytd'] = $ytdRecords[$key]['count'];
            $monthlyActivity[$key]['sum_of_transaction_ytd'] = $ytdRecords[$key]['sum'];
        }

        return $monthlyActivity;
        
    }

    /**
     * 
     * Calculate YTD 
     * @return calculated YTD
     */
    private function __calculateYTD($yearlyActivities)
    {
        $yearlyRecord = array();
        
        $count = 0;
        
        foreach ($yearlyActivities as $keyAct => $activities)
        {
            foreach($activities as $activity) {
                $merchants[] = $activity['merchantId'];
            }
            break;
        }
        
        foreach ($merchants as $newkey)
        {
            if(strlen($newkey) == 2) {
                $key = '00'.$newkey;
            }
            if(strlen($newkey) == 3) {
                $key = '0'.$newkey;
            } 
            if(strlen($newkey) > 3) {
                $key = $newkey;
            }
            $yearlyRecord[$key] = array();
            $yearlyRecord[$key]['count'] = 0;
            $yearlyRecord[$key]['sum'] = 0;
            foreach ($yearlyActivities as $keyAct => $activities)
            {   
                if(!array_key_exists($count, $activities)) {
                    $activities[$count]['txn_count'] = 0;
                    $activities[$count]['txn_sum'] = 0;
                }
                $yearlyRecord[$key]['count'] += $activities[$count]['txn_count'];
                $yearlyRecord[$key]['sum'] += $activities[$count]['txn_sum'];
            }
            
            $count++;
        }
        

        return $yearlyRecord;
    }

}
