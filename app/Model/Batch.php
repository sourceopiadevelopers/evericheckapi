<?php

App::uses('AppModel', 'Model');
App::uses('RequestClass', 'Model');
App::uses('Calculation', 'Model');

class Batch extends AppModel {

    /**
     * 
     * Constructor for Batch Model
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->RequestClass = new RequestClass();
        $this->Calculation = new Calculation();
    }

    /**
     * 
     * Fetch all batches
     * @return type array containing all batches
     */
    public function fetchBatches($data) {
        $param = $this->Calculation->getParams($data);
        $response = $this->RequestClass->getBatches($param);
        
        if (!isset($response['success'])) {
            $response = array('success' => TRUE, 'payload' => $response);
        }
        
        return $response;
        
    }

}
