<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');
App::uses('ResponseClass', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model
{

    /**
     * Transaction status either 'true' or 'false'
     * 
     * @var String
     */
    protected $_success;

    /**
     * Contains array from find()
     * 
     * @var Array
     */
    protected $_payload;

    /**
     *
     * @var type 
     */
    protected $_error;

    /**
     * Error code for the transaction
     * 
     * @var type 
     */
    protected $_code;

    /**
     * Error message for the transaction
     * 
     * @var String 
     */
    protected $_message;

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);

        $this->_success = FALSE;

        $this->_payload = NULL;

        $this->_message = NULL;

        $this->_code = NULL;
    }

    /**
     * Calls for Response Class's Success function
     * 
     * @param array $condition condition to fetch from table
     * @param object $obj obect of which data to be fetched
     * @param array $fields list of field which should be fetched
     * @return array response array with success status and payload
     */
    protected function _requestSuccess($condition, $obj, $fields = null)
    {
        $this->ResponseClass = new ResponseClass();
        return $this->ResponseClass->success($condition, $obj, $fields);
    }

    /**
     * Calls for Response Class's SuccessMessage function
     * 
     * @param string $message success message
     * @return array response array with success status and message
     */
    protected function _requestSuccessMessage($message)
    {
        $this->ResponseClass = new ResponseClass();
        return $this->ResponseClass->successMessage($message);
    }

    /**
     * Calls for Response Class's error function
     * 
     * @param string $error error message
     * @return array response array with success status and error message
     */
    protected function _requestError($error)
    {
        $this->ResponseClass = new ResponseClass();
        return $this->ResponseClass->error($error);
    }

    /**
     * Calls for Response Class's invalidError function
     * 
     * @param array $invalidError list of error caugth by validation
     * @return array response array with success status and invalid errors message
     */
    protected function _requestInvalidError($invalidError)
    {
        $this->ResponseClass = new ResponseClass();
        return $this->ResponseClass->getInvalidError($invalidError);
    }

    protected function _getUser()
    {
        $apacheHeader = apache_request_headers();
        if (array_key_exists('Authorization', $apacheHeader))
        {
            $header = explode(' ', $apacheHeader['Authorization']);
            $token = $header[1];
            $decoded = JWT::decode($token, Configure::read('Security.salt'));
            return $decoded;
        }
    }
    
    


}
