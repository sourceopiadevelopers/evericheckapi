<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');
App::uses('RequestClass', 'Model');

class Report extends AppModel {

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        $this->RequestClass = new RequestClass();
    }

    private function __getParams($data) {
        $values = [];
        foreach ($data as $key => $value) {
            $values[] = $key . '=' . $value;
        }
        if (count($values) > 1) {
            $param = implode('&', $values);
        } else {
            $param = $values[0];
        }
        return $param;
    }

    public function fetchBatches($data) {
        $param = $this->__getParams($data);
        $payload = $this->RequestClass->getBatches($param);
        return array('success' => TRUE, 'payload' => $payload);
    }

    /**
     * 
     * @return type
     */
    public function fetchIsos() {
        $payload = $this->RequestClass->getIsos();
        return array('success' => TRUE, 'payload' => $payload);
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public function fetchIsosById($data) {
        $param = $this->__getParams($data);
        $payload = $this->RequestClass->getIsosById($param);
        return array('success' => TRUE, 'payload' => $payload);
    }

    /**
     * 
     * @return type
     */
    public function fetchMerchants() {
        $payload = $this->RequestClass->getMerchants();
        return array('success' => TRUE, 'payload' => $payload);
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public function fetchMerchantsById($data) {
        $param = $this->__getParams($data);
        $payload = $this->RequestClass->getMerchantsById($param);
        return array('success' => TRUE, 'payload' => $payload);
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public function fetchMerchantsByIsoId($data) {
        $param = $this->__getParams($data);
        $payload = $this->RequestClass->getMerchantsByIsoId($param);
        return array('success' => TRUE, 'payload' => $payload);
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public function fetchMonthlyMerchantActivity($data) {
        $param = $this->__getParams($data);
        $merchantRaw = $this->RequestClass->getMonthlyMerchantActivity($param);
        $payload = $this->__formatData($merchantRaw);
        return array('success' => TRUE, 'payload' => $payload);
    }

    private function __formatData($data) {
        $count = 0;
        foreach ($data as $key => $value) {
            $formattedData[$count] = $value;
            $formattedData[$count]['month'] = $key;
            $count++;
        }
        return $formattedData;
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public function fetchMonthlyIsoActivity($data) {
        $param = $this->__getParams($data);
        $isosRaw = $this->RequestClass->getMonthlyIsoActivity($param);
        $payload = $this->__formatData($isosRaw);
        return array('success' => TRUE, 'payload' => $payload);
    }

    /**
     * 
     * @param type $isoId
     * @return type
     */
    public function fetchIsoMerchantsCurrentActivity($isoId) {
        $allIsoMerchants = $this->RequestClass->getAllIsoMerchants($isoId);
        return $this->__fetchEachMerchantCurrentActivity($allIsoMerchants);
    }

    /**
     * 
     * @param type $paramNew, $paramOld
     * @return type
     */
    private function __calculateVariance($paramNew, $paramOld) {
        if ($paramNew == 0 && $paramOld == 0) {
            return 0;
        }

        return ((($paramNew - $paramOld) / $paramOld) * 100);
    }

    /**
     * 
     * @param type $paramNew, $paramOld
     * @return type
     */
    private function __calculateGain($paramNew, $paramOld) {
        if ($paramNew == 0 && $paramOld == 0) {
            return 0;
        }
        return (($paramNew / $paramOld) * 100);
    }

    /**
     * 
     * @param type $paramNew, $paramOld
     * @return type
     */
    private function __calculateDifference($paramNew, $paramOld) {
        return ($paramNew - $paramOld);
    }

    /**
     * 
     * @param type $isoMerchantData
     * @return type
     */
    private function __fetchEachMerchantCurrentActivity($isoMerchantData) {
        $endDate = date('Y-m-d');
        $startDate = date('Y-m-j', strtotime("first day of previous month"));
        $dataToBeSent = array();
        $records = array();
        $count = 0;
        foreach ($isoMerchantData as $eachMerchantId => $value) {
            $dataToBeSent = array('start_date' => $startDate,
                'end_date' => $endDate,
                'merchant' => $eachMerchantId);
            $param = $this->__getParams($dataToBeSent);
            $value['id'] = $eachMerchantId;
            $activity = $this->RequestClass->getMonthlyMerchantActivity($param);
            $records[$count] = $this->__generateActivity($value, $activity);
            $count++;
        }
        return array('success' => TRUE, 'payload' => $records);
    }

    private function __generateActivity($merchant, $activity) {
        $current = date('Y-m');
        $last = date('Y-m', strtotime("first day of previous month"));
        $recordYTD = $this->__calculateYTD($merchant['id'], $current);
        $monthlyActivity = array();
        $monthlyActivity['merchant_id'] = $merchant['id'];
        $monthlyActivity['merchant_name'] = $merchant['name'];
        $monthlyActivity['last_txn_mtd_count'] = $activity[$last]['mtd_txn_count'];
        $monthlyActivity['current_txn_mtd_count'] = $activity[$current]['mtd_txn_count'];
        $monthlyActivity['number_of_transaction_variance'] = $this->__calculateVariance($monthlyActivity['current_txn_mtd_count'], $monthlyActivity['last_txn_mtd_count']);
        $monthlyActivity['number_of_transaction_gain'] = $this->__calculateGain($monthlyActivity['current_txn_mtd_count'], $monthlyActivity['last_txn_mtd_count']);
        $monthlyActivity['number_of_transaction_difference'] = $this->__calculateDifference($monthlyActivity['current_txn_mtd_count'], $monthlyActivity['last_txn_mtd_count']);
        $monthlyActivity['last_mtd_txn_sum'] = $activity[$last]['mtd_txn_sum'];
        $monthlyActivity['current_mtd_txn_sum'] = $activity[$current]['mtd_txn_sum'];
        $monthlyActivity['sum_of_transaction_variance'] = $this->__calculateVariance($monthlyActivity['current_mtd_txn_sum'], $monthlyActivity['last_mtd_txn_sum']);
        $monthlyActivity['sum_of_transaction_gain'] = $this->__calculateGain($monthlyActivity['current_mtd_txn_sum'], $monthlyActivity['last_mtd_txn_sum']);
        $monthlyActivity['sum_of_transaction_difference'] = $this->__calculateDifference($monthlyActivity['current_mtd_txn_sum'], $monthlyActivity['last_mtd_txn_sum']);
        $monthlyActivity['txn_sum'] = $activity[$last]['txn_sum'];
        $monthlyActivity['txn_count'] = $activity[$last]['txn_count'];
        $monthlyActivity['number_of_transaction_ytd'] = $recordYTD['count'];
        $monthlyActivity['sum_of_transaction_ytd'] = $recordYTD['sum'];
        return $monthlyActivity;
    }

    private function __calculateYTD($id, $currentDate) {
        $yearlyRecord = array('count' => 0, 'sum' => 0);
        $yearsFirstDate = date('Y-01');
        $dataToBeSent = array(
            'start_date' => $yearsFirstDate,
            'end_date' => $currentDate,
            'merchant' => $id);
        $param = $this->__getParams($dataToBeSent);
        $yearlyActivity = $this->RequestClass->getMonthlyMerchantActivity($param);

        foreach ($yearlyActivity as $activity) {
            $yearlyRecord['count'] += $activity['txn_count'];
            $yearlyRecord['sum'] += $activity['txn_sum'];
        }

        return $yearlyRecord;
    }

    /**
     * 
     * @return type
     */
    public function fetchIsoCurrentMonthlyActivity() {
        $allIsos = $this->RequestClass->getIsos();
        return $this->__fetchEachIsoCurrentMonthlyActivity($allIsos);
    }

    /**
     * 
     * @param type $isoData
     * @return type
     */
    private function __fetchEachIsoCurrentMonthlyActivity($isoData) {
        $endDate = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime("first day of -1 month"));
        $dataToBeSent = array();
        $records = array();
        $count = 0;

        foreach ($isoData as $eachIsoId => $value) {
            $dataToBeSent = array('start_date' => $startDate,
                'end_date' => $endDate,
                'iso' => $eachIsoId);
            $param = $this->__getParams($dataToBeSent);
            $value['id'] = $eachIsoId;
            $activity = $this->RequestClass->getMonthlyIsoActivity($param);
//            pr($activity);die;
            $records[$count] = $this->__generateIsoMonthlyActivity($value, $activity);
            $count++;
        }
        return array('success' => TRUE, 'payload' => $records);
    }

    private function __generateIsoMonthlyActivity($isos, $activity) {
        $current = date('Y-m');
        $last = date('Y-m', strtotime("first day of previous month"));
        $monthlyIsoActivity = array();
        $monthlyIsoActivity['iso_id'] = $isos['id'];
        
        $monthlyIsoActivity['last_mtd_new_merchants_count'] = $activity[$last]['mtd_merchants_new_count'];
        $monthlyIsoActivity['current_mtd_new_merchants_count'] = $activity[$current]['mtd_merchants_new_count'];
        $monthlyIsoActivity['mtd_new_merchants_count_variance'] = $this->__calculateVariance($monthlyIsoActivity['current_mtd_new_merchants_count'],$monthlyIsoActivity['last_mtd_new_merchants_count']);
        $monthlyIsoActivity['mtd_new_merchants_count_gain'] = $this->__calculateGain($monthlyIsoActivity['current_mtd_new_merchants_count'],$monthlyIsoActivity['last_mtd_new_merchants_count']);
        $monthlyIsoActivity['mtd_new_merchants_count_difference'] = $this->__calculateDifference($monthlyIsoActivity['current_mtd_new_merchants_count'],$monthlyIsoActivity['last_mtd_new_merchants_count']);
        
        $monthlyIsoActivity['last_mtd_merchants_closed_count'] = $activity[$last]['mtd_merchants_closed_count'];
        $monthlyIsoActivity['current_mtd_merchants_closed_count'] = $activity[$current]['mtd_merchants_closed_count'];
        $monthlyIsoActivity['mtd_closed_merchants_count_variance'] = $this->__calculateVariance($activity[$current]['mtd_merchants_closed_count'],$activity[$last]['mtd_merchants_closed_count']);
        $monthlyIsoActivity['mtd_closed_merchants_count_gain'] = $this->__calculateGain($activity[$current]['mtd_merchants_closed_count'],$activity[$last]['mtd_merchants_closed_count']);
        $monthlyIsoActivity['mtd_closed_merchants_count_difference'] = $this->__calculateDifference($activity[$current]['mtd_merchants_closed_count'],$activity[$last]['mtd_merchants_closed_count']);

        $monthlyIsoActivity['last_mtd_txn_count'] = $activity[$last]['mtd_txn_count'];
        $monthlyIsoActivity['current_mtd_txn_count'] = $activity[$current]['mtd_txn_count'];
        $monthlyIsoActivity['mtd_txn_count_variance'] = $this->__calculateVariance($monthlyIsoActivity['current_mtd_txn_count'],$monthlyIsoActivity['last_mtd_txn_count']); 
        $monthlyIsoActivity['mtd_txn_count_gain'] = $this->__calculateGain($monthlyIsoActivity['current_mtd_txn_count'],$monthlyIsoActivity['last_mtd_txn_count']); 
        $monthlyIsoActivity['mtd_txn_count_difference'] = $this->__calculateDifference($monthlyIsoActivity['current_mtd_txn_count'],$monthlyIsoActivity['last_mtd_txn_count']); 
        
        $monthlyIsoActivity['last_mtd_txn_sum'] = $activity[$last]['mtd_txn_sum'];
        $monthlyIsoActivity['current_mtd_txn_sum'] = $activity[$current]['mtd_txn_sum'];
        $monthlyIsoActivity['mtd_txn_sum_variance'] = $this->__calculateVariance($monthlyIsoActivity['last_mtd_txn_sum'], $monthlyIsoActivity['current_mtd_txn_sum']);
        $monthlyIsoActivity['mtd_txn_sum_gain'] = $this->__calculateGain($monthlyIsoActivity['current_mtd_txn_sum'],$monthlyIsoActivity['last_mtd_txn_sum']); 
        $monthlyIsoActivity['mtd_txn_sum_difference'] = $this->__calculateDifference($monthlyIsoActivity['current_mtd_txn_sum'],$monthlyIsoActivity['last_mtd_txn_sum']); 

        return $monthlyIsoActivity;
    }

    public function fetchMerchantCurrentMonthlyActivity() {
        $endDate = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime("first day of -1 month"));

        $records = array();
        $count = 0;
        $merchantCurrentDayToSent = array();
        $merchants = $this->fetchMerchants();
        foreach ($merchants['payload'] as $merchantId => $value) {
            $merchantCurrentDayToSent = array('start_date' => $startDate,
                'end_date' => $endDate,
                'merchant' => $merchantId);
            $param = $this->__getParams($merchantCurrentDayToSent);
            $value['id'] = $merchantId;
            $activity = $this->RequestClass->getMonthlyMerchantActivity($param);
            $records[$count] = $this->__generateActivity($value, $activity);
            $count++;
        }
        return array('success' => TRUE, 'payload' => $records);
    }

}
