<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 * @property CakeRequest $CakeRequest
 * @property OldPassword $OldPassword
 * @property SearchFilter $SearchFilter
 * @property ServerRequest $ServerRequest
 * @property UserNotice $UserNotice
 */
class User extends AppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';

    public $actsAs = array('Acl' => array('type' => 'requester'));

    /**
     * Role inserted as ARO's and acts as a parent for other users belonging to Role.
     * 
     * @return null
     */
    public function parentNode()
    {
        return null;
    }
    /**
     * 
     * @param type $id
     * @param type $table
     * @param type $ds
     */
    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }
    

    public function beforeSave($options = null)
    {
        $passwordHasher = new SimplePasswordHasher();
        $this->data['User']['password'] = $passwordHasher->hash($this->data['User']['password']);
        return true;
    }
    
    /**
     * 
     * @param type $user
     * @return type\
     */
    public function getLoginCredential($user)
    {
        $passwordHasher = new SimplePasswordHasher();
        return $this->find('first', array('conditions' => array(
                        'username' => $user['name'],
                        'password' => $passwordHasher->hash($user['password'])
        )));
    }

    /**
     * 
     * @param type $user
     */
    public function add($user)
    {
        return $this->save($user);
    }

    /**
     * 
     * @return type
     */
    public function fetch()
    {
        return $this->_requestSuccess(array(), $this);
    }

}
