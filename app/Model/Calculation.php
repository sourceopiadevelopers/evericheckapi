<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Calculation extends Model {
    
    
    
    /**
     * 
     * Append params for sending to data_layer as request query
     * @return appended params in request query pattern
     */
      public function getParams($data) {
        $values = [];
        
        if(is_array($data)){
            
        foreach ($data as $key => $value) {
            $values[] = $key . '=' . $value;
        }
        
        if (count($values) > 1) {
            $param = implode('&', $values);
        }
        
        else {
            $param = $values[0];
        }
        
        return $param;
        
        }
    }
    
    
    
    /**
     * 
     * Format final data to be sent to requesting applications
     * @return formatted Data
     */
      public function formatData($data) {
        $count = 0;
        
        if (count($data) !== count($data, COUNT_RECURSIVE)){ 
            
        foreach ($data as $key => $value) {
            $formattedData[$count] = $value;
            $formattedData[$count]['month'] = $key;
            $count++;
        }
        
        return $formattedData;
        
      }
    }
    
    
    
    /**
     * 
     * Calculate Variance
     * @return calculated variance
     */
    public function calculateVariance($paramNew, $paramOld) {
        if ($paramOld==0) {
            $return = 0;
        }else{
            $return =  ((($paramNew - $paramOld) / $paramOld) * 100);
        }
        return $return;
    }
    
    
    

    /**
     * 
     * Calculate Gain
     * @return calculated gain
     */
    public function calculateGain($paramNew, $paramOld) {
        if ($paramOld==0) {
            $return = 0;
        }else{
            $return = (($paramNew / $paramOld) * 100);
        }
        return $return;
    }
    
    
    

    /**
     * 
     * Calculate Difference
     * @return calculated difference
     */
    public function calculateDifference($paramNew, $paramOld) {
        return ($paramNew - $paramOld);
    }
    


    
}







?>
