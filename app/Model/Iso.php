<?php

App::uses('AppModel', 'Model');
App::uses('RequestClass', 'Model');
App::uses('Calculation', 'Model');

class Iso extends AppModel {

    /**
     * 
     * Constructor for Iso Model class
     * 
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        
        $this->RequestClass = new RequestClass();
        $this->Calculation = new Calculation();
    }

    /**
     * Get all Isos
     *  
     * @return array containing all Isos
     */
    public function getIsos() {
        $allIsos = $this->RequestClass->getIsos();

        if (!isset($allIsos['success'])) {
            $allIsos = array('success' => TRUE, 'payload' => $allIsos);
        }

        return $allIsos;
        
    }

    /**
     * Get All Isos By Id
     * 
     * @param type $data
     * @return type
     */
    public function fetchIsosById($data) {
        $param = $this->Calculation->getParams($data);
        $response = $this->RequestClass->getIsosById($param);

        if (!isset($response['success'])) {
            $response = array('success' => TRUE, 'payload' => $response);
        }

        return $response;
        
    }

    /**
     * 
     * Get Monthly Iso Activity
     * @return array containing monthly iso activity
     */
    public function fetchMonthlyIsoActivity($data) {
        $param = $this->Calculation->getParams($data);
        $isosRaw = $this->RequestClass->getMonthlyIsoActivity($param);
        
        if (!isset($isosRaw['success']))  {

            if (!empty($isosRaw)) {
                $isosRaw = $this->Calculation->formatData($isosRaw);
            }

            $isosRaw = array('success' => true, 'payload' => $isosRaw);
        }

        return $isosRaw;
        
    }

    /**
     * 
     * Get current month activity for all merchants
     * @return type array containing current month activity of every isos
     */
    public function fetchIsoCurrentMonthlyActivity() {
        $allIsos = $this->RequestClass->getIsos();
        $response = array();

        if (!isset($allIsos['success'])) {
            
           if(!empty($allIsos)){ 
           $response = $this->__fetchEachIsoCurrentMonthlyActivity($allIsos);
           }
           
        }
        
        return $response;
        
    }

    /**
     * 
     * Get current month activity for each merchant
     * @return type array containing current month activity of every isos
     */
    private function __fetchEachIsoCurrentMonthlyActivity($isoData) {
        $endDate = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime("first day of -1 month"));
        $dataToBeSent = array();
        $records = array();
        $count = 0;

        foreach ($isoData as $eachIsoId => $value) {
            $dataToBeSent = array('start_date' => $startDate,
                'end_date' => $endDate,
                'iso' => $eachIsoId);
            $param = $this->Calculation->getParams($dataToBeSent);
            $value['id'] = $eachIsoId;
            $activity = $this->RequestClass->getMonthlyIsoActivity($param);

            if(!isset($activity['success'])){
            $records[$count] = $this->__generateIsoMonthlyActivity($value, $activity);
            $count++;
            }
        }

        return array('success' => TRUE, 'payload' => $records);
        
    }

    /**
     * 
     * Generate Iso Monthly Activity
     * @return type array containing current month activity for each merchant
     */
    private function __generateIsoMonthlyActivity($isos, $activity) {
        $keys = array_keys($activity);
        $current = $keys[1];
        $last = $keys[0];
        $monthlyIsoActivity = array();
        $monthlyIsoActivity['iso_id'] = $isos['id'];
        $monthlyIsoActivity['last_mtd_new_merchants_count'] = ($activity[$last]['mtd_merchants_new_count'] != null ? $activity[$last]['mtd_merchants_new_count']: 0);
        $monthlyIsoActivity['current_mtd_new_merchants_count'] = ($activity[$current]['mtd_merchants_new_count'] != null ? $activity[$current]['mtd_merchants_new_count']:0);
        $monthlyIsoActivity['mtd_new_merchants_count_variance'] = $this->Calculation->calculateVariance($monthlyIsoActivity['current_mtd_new_merchants_count'], $monthlyIsoActivity['last_mtd_new_merchants_count']);
        $monthlyIsoActivity['mtd_new_merchants_count_gain'] = $this->Calculation->calculateGain($monthlyIsoActivity['current_mtd_new_merchants_count'], $monthlyIsoActivity['last_mtd_new_merchants_count']);
        $monthlyIsoActivity['mtd_new_merchants_count_difference'] = $this->Calculation->calculateDifference($monthlyIsoActivity['current_mtd_new_merchants_count'], $monthlyIsoActivity['last_mtd_new_merchants_count']);

        $monthlyIsoActivity['last_mtd_merchants_closed_count'] = ($activity[$last]['mtd_merchants_closed_count'] != null ? $activity[$last]['mtd_merchants_closed_count'] :0 );
        $monthlyIsoActivity['current_mtd_merchants_closed_count'] = ($activity[$current]['mtd_merchants_closed_count'] != null ? $activity[$current]['mtd_merchants_closed_count'] : 0);
        $monthlyIsoActivity['mtd_closed_merchants_count_variance'] = $this->Calculation->calculateVariance($activity[$current]['mtd_merchants_closed_count'], $activity[$last]['mtd_merchants_closed_count']);
        $monthlyIsoActivity['mtd_closed_merchants_count_gain'] = $this->Calculation->calculateGain($activity[$current]['mtd_merchants_closed_count'], $activity[$last]['mtd_merchants_closed_count']);
        $monthlyIsoActivity['mtd_closed_merchants_count_difference'] = $this->Calculation->calculateDifference($activity[$current]['mtd_merchants_closed_count'], $activity[$last]['mtd_merchants_closed_count']);

        $monthlyIsoActivity['last_mtd_txn_count'] = ($activity[$last]['mtd_txn_count'] != null ? $activity[$last]['mtd_txn_count'] : 0);
        $monthlyIsoActivity['current_mtd_txn_count'] = ($activity[$current]['mtd_txn_count'] != null ? $activity[$current]['mtd_txn_count']:0);
        $monthlyIsoActivity['mtd_txn_count_variance'] = $this->Calculation->calculateVariance($monthlyIsoActivity['current_mtd_txn_count'], $monthlyIsoActivity['last_mtd_txn_count']);
        $monthlyIsoActivity['mtd_txn_count_gain'] = $this->Calculation->calculateGain($monthlyIsoActivity['current_mtd_txn_count'], $monthlyIsoActivity['last_mtd_txn_count']);
        $monthlyIsoActivity['mtd_txn_count_difference'] = $this->Calculation->calculateDifference($monthlyIsoActivity['current_mtd_txn_count'], $monthlyIsoActivity['last_mtd_txn_count']);

        $monthlyIsoActivity['last_mtd_txn_sum'] = ($activity[$last]['mtd_txn_sum'] != null ? $activity[$last]['mtd_txn_sum'] : 0);
        $monthlyIsoActivity['current_mtd_txn_sum'] = ($activity[$current]['mtd_txn_sum'] != null ? $activity[$current]['mtd_txn_sum'] : 0);
        $monthlyIsoActivity['mtd_txn_sum_variance'] = $this->Calculation->calculateVariance($monthlyIsoActivity['last_mtd_txn_sum'], $monthlyIsoActivity['current_mtd_txn_sum']);
        $monthlyIsoActivity['mtd_txn_sum_gain'] = $this->Calculation->calculateGain($monthlyIsoActivity['current_mtd_txn_sum'], $monthlyIsoActivity['last_mtd_txn_sum']);
        $monthlyIsoActivity['mtd_txn_sum_difference'] = $this->Calculation->calculateDifference($monthlyIsoActivity['current_mtd_txn_sum'], $monthlyIsoActivity['last_mtd_txn_sum']);

        return $monthlyIsoActivity;
        
    }

    /**
     * 
     * Fetch Isos Merchants Current Month Activity
     * @return type array containing current month activity of all merchants of iso with id $isoId
     */
    public function fetchIsoMerchantsCurrentActivity($isoId) {
        $allIsoMerchants = $this->RequestClass->getAllIsoMerchants($isoId);
        $response = array();

        if (!isset($allIsoMerchants['success'])) {
            
           if(!empty($allIsoMerchants)){ 
           $response = $this->__fetchEachMerchantCurrentActivity($allIsoMerchants);
           }
           
        }
        
        return $response;
        
    }

    /**
     * 
     * Fetch Isos Merchant Current Month Activity one by one
     * @return type array containing current month activity of all merchants of the iso.
     */
    private function __fetchEachMerchantCurrentActivity($isoMerchantData) {
        $endDate = date('Y-m-d');
        $startDate = date('Y-m-j', strtotime("first day of previous month"));
        $dataToBeSent = array();
        $records = array();
        $count = 0;

        foreach ($isoMerchantData as $eachMerchantId => $value) {
            $dataToBeSent = array('start_date' => $startDate,
                'end_date' => $endDate,
                'merchant' => $eachMerchantId);
            $param = $this->Calculation->getParams($dataToBeSent);
            $value['id'] = $eachMerchantId;
            $activity = $this->RequestClass->getMonthlyMerchantActivity($param);
            
            if(!isset($activity['success']) && !empty($activity)){
            $records[$count] = $this->__generateActivity($value, $activity);
            $count++;
            }
        }

        return array('success' => TRUE, 'payload' => $records);
        
    }

    /**
     * Get Current Month Activity for each merchant
     * @return type array containing current month activity of each merchant
     */
    private function __generateActivity($merchant, $activity) {
        $keys = array_keys($activity);
        $current = $keys[1];
        $last = $keys[0];
        $recordYTD = $this->__calculateYTD($merchant['id'], $current);
        $monthlyActivity = array();
        $monthlyActivity['merchant_id'] = $merchant['id'];
        $monthlyActivity['merchant_name'] = $merchant['name'];
        $monthlyActivity['last_txn_mtd_count'] = $activity[$last]['mtd_txn_count'];
        $monthlyActivity['current_txn_mtd_count'] = $activity[$current]['mtd_txn_count'];
        $monthlyActivity['number_of_transaction_variance'] = $this->Calculation->calculateVariance($monthlyActivity['current_txn_mtd_count'], $monthlyActivity['last_txn_mtd_count']);
        $monthlyActivity['number_of_transaction_gain'] = $this->Calculation->calculateGain($monthlyActivity['current_txn_mtd_count'], $monthlyActivity['last_txn_mtd_count']);
        $monthlyActivity['number_of_transaction_difference'] = $this->Calculation->calculateDifference($monthlyActivity['current_txn_mtd_count'], $monthlyActivity['last_txn_mtd_count']);
        $monthlyActivity['last_mtd_txn_sum'] = $activity[$last]['mtd_txn_sum'];
        $monthlyActivity['current_mtd_txn_sum'] = $activity[$current]['mtd_txn_sum'];
        $monthlyActivity['sum_of_transaction_variance'] = $this->Calculation->calculateVariance($monthlyActivity['current_mtd_txn_sum'], $monthlyActivity['last_mtd_txn_sum']);
        $monthlyActivity['sum_of_transaction_gain'] = $this->Calculation->calculateGain($monthlyActivity['current_mtd_txn_sum'], $monthlyActivity['last_mtd_txn_sum']);
        $monthlyActivity['sum_of_transaction_difference'] = $this->Calculation->calculateDifference($monthlyActivity['current_mtd_txn_sum'], $monthlyActivity['last_mtd_txn_sum']);
        $monthlyActivity['txn_sum'] = $activity[$last]['txn_sum'];
        $monthlyActivity['txn_count'] = $activity[$last]['txn_count'];
        $monthlyActivity['number_of_transaction_ytd'] = $recordYTD['count'];
        $monthlyActivity['sum_of_transaction_ytd'] = $recordYTD['sum'];

        return $monthlyActivity;
        
    }

    /**
     * Calculating YTD for current month activities of iso
     * 
     * @return type YTD calculated data
     */
    private function __calculateYTD($id, $currentDate) {
        $yearlyRecord = array('count' => 0, 'sum' => 0);
        $yearsFirstDate = date('Y-01');
        $dataToBeSent = array(
            'start_date' => $yearsFirstDate,
            'end_date' => $currentDate,
            'merchant' => $id);
        $param = $this->Calculation->getParams($dataToBeSent);
        $yearlyActivity = $this->RequestClass->getMonthlyMerchantActivity($param);

        if(!isset($yearlyActivity['success']) && !empty($yearlyActivity)){
            
        foreach ($yearlyActivity as $activity) {
            $yearlyRecord['count'] += $activity['txn_count'];
            $yearlyRecord['sum'] += $activity['txn_sum'];
        }
        
        }

        return $yearlyRecord;
        
    }

}
