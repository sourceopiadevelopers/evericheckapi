<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class ResponseClass extends AppModel
{

    /**
     * 
     * @param type $id
     * @param type $table
     * @param type $ds
     */
    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
    }

    /**
     * 
     * @param type $condition
     * @param type $model
     * @param type $fields
     * @param type $joins
     * @return type
     */
    public function success($condition, $model, $fields = null, $joins = null)
    {
        $this->setPayload($condition, $model, $fields, $joins);
        $this->setSuccess('true');

        return array('success' => $this->_success, 'payload' => $this->_payload);
    }

    /**
     * 
     * @param type $condition
     * @param type $model
     * @param type $fields
     * @param type $joins
     * @return type
     */
    public function successMessage($message)
    {
        $this->setMessage($message);
        $this->setSuccess('true');
        return array('success' => $this->_success, 'message' => $this->_message);
    }

    /**
     * 
     * @param type $user
     * @return type
     */
    public function successLogin($user)
    {
        $token = JWT::encode($user, Configure::read('Security.salt'));
        $this->setSuccess('true');
        return array('success' => $this->_success,'id' => $user['id'] ,
            'token' => $token);
    }

    /**
     * 
     * @param type $message
     * @return type
     */
    public function error($message)
    {
        $this->setMessage($message);
        $this->setSuccess('false');
        return array('success' => $this->_success, 'message' => $this->_message);
    }

    /**
     * 
     * @param type $invalidError
     * @return type
     */
    public function getInvalidError($invalidError = array())
    {
        $falt = Hash::flatten($invalidError);
        $single = array();
        $orgKey = '';
        $errorMessage = '';
        foreach ($falt as $key => $val)
        {
            $keyExploded = explode('.', $key);
            $orgKey = count($keyExploded) === 2 ? $keyExploded[0] : $keyExploded[1];
            if (!\array_key_exists($orgKey, $single))
            {
                $single[$orgKey] = $val;
                $errorMessage .= $orgKey . ' : ' . $val . " | ";
            }
        }
        return substr($errorMessage, 0, -3);
    }

    /**
     * 
     * @param type $conditions
     * @param type $model
     * @param type $fields
     * @param type $joins
     */
    public function setPayload($conditions, $model, $fields = null, $joins = null)
    {
        $filterConditions = empty($conditions) ? array() :
                array('conditions' => $conditions);

        $fields = empty($fields) ? array() : array('fields' => $fields);
        $joins = empty($joins) ? array() : array('joins' => $joins);
        $conditionparams = array_merge($filterConditions, $fields);
        $params = array_merge($conditionparams, $joins);

        $this->_payload = $model->find('all', $params);
    }

    /**
     * Get $_payload.
     * 
     * @return $_payload
     */
    public function getPayload()
    {
        return $this->_payload;
    }

    /**
     * Set $_success.
     * 
     * @param String $success
     */
    public function setSuccess($success)
    {
        if ($success == 'false')
        {
            $this->_success = FALSE;
        } else
        {
            $this->_success = TRUE;
        }
    }

    /**
     * Get $_success
     * 
     * @return $this->_success
     */
    public function getSuccess()
    {
        return $this->_success;
    }

    /**
     * Set $_message.
     * 
     * @param String $message
     */
    public function setMessage($message)
    {
        $this->_message = $message;
    }

    /**
     * Get $_message.
     * 
     * @return $_message
     */
    public function getMessage()
    {
        return $this->_message;
    }

}
