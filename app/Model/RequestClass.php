<?php

App::uses('AppModel', 'Model');

class RequestClass extends AppModel {

    private $__apiUrl = 'https://svn.evericheck.com:81';

    //private $__apiUrl = 'https://www.evericheck.com/datalayer_dev/Controller';

    /**
     * 
     * Initiating curl requests for given url
     * @return type array containing response from data layer
     */
    private function __initiateGETRequest($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $response;
    }

    /**
     * 
     * Get batches from data_layer
     * @return type array containing batches
     */
    public function getBatches($data) {
        $url = $this->__apiUrl . '/Batches.php?' . $data;
        return $this->__initiateGETRequest($url);
    }

    /**
     * 
     * Get isos from data_layer
     * @return type array containing isos
     */
    public function getIsos() {
        $url = $this->__apiUrl . '/ISOs.php';
        return $this->__initiateGETRequest($url);
    }

    /**
     * 
     * Get isos using given id from data_layer
     * @return type array containing isos
     */
    public function getIsosById($data) {
        $url = $this->__apiUrl . '/ISOs.php?' . $data;
        return $this->__initiateGETRequest($url);
    }

    /**
     * Get merchants from data_layer
     * @return type array containing all merchants
     */
    public function getMerchants() {
        $url = $this->__apiUrl . '/Merchants.php';
        return $this->__initiateGETRequest($url);
    }
    
        /**
     * Get merchants from data_layer
     * @return type array containing all merchants
     */
    public function getMerchantsByPage($page, $active) {
        $url = $this->__apiUrl . '/Merchants.php?'.$page.'&active='.$active;
        return $this->__initiateGETRequest($url);
    }

    /**
     * 
     * Get merchants by params from data_layer
     * @return type array containing merchants
     */
    public function getMerchantsByParams($data) {
        $url = $this->__apiUrl . '/Merchants.php?' . $data;
        return $this->__initiateGETRequest($url);
    }

    /**
     * 
     * Get monthly activity of merchant from data_layer
     * @return type array containing merchant monthly activity reports
     */
    public function getMonthlyMerchantActivity($data) {
        $url = $this->__apiUrl . '/MerchantMonthlyActivity.php?' . $data;
        return $this->__initiateGETRequest($url);
    }

    /**
     * 
     * Get monthly activity of iso from data_layer
     * @return type array containing iso monthly activity reports
     */
    public function getMonthlyIsoActivity($data) {
        $url = $this->__apiUrl . '/ISOMonthlyActivity.php?' . $data;
        return $this->__initiateGETRequest($url);
    }

    /**
     * 
     * Get all merchants of particular Iso
     * @return type array containing all merchants of iso
     */
    public function getAllIsoMerchants($isoId) {
        $url = $this->__apiUrl . '/Merchants.php?iso=' . $isoId['iso'];
        return $this->__initiateGETRequest($url);
    }

}
