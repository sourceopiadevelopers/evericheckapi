
#!/bin/bash

echo "export evericheck_db_default_host=localhost">>/etc/environment
echo "export evericheck_db_default_user=root">>/etc/environment
echo "export evericheck_db_default_password=root">>/etc/environment
echo "export evericheck_core_debug=2">>/etc/environment
echo "export evericheck_core_security_salt='Evericheck Version 2'">>/etc/environment
echo "export evericheck_core_chipher_seed=1200">>/etc/environment
echo "export evericheck_api_url=http://localhost/EvericheckAPI">>/etc/environment
echo "export evericheck_client_url=eveicheck.client.com">>/etc/environment

echo "do you wanna restart your computer to apply changes in /etc/environment file? yes(y)no(n)"
read restart
case $restart in
    y) sudo shutdown -r 0;;
    n) echo "don't forget to restart your computer manually";;
esac
exit