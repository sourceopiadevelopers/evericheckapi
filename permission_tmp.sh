
#!/bin/bash

DIR='/var/www/html/EvericheckAPI'

chmod u+x ${DIR}/permission_tmp.sh

mkdir -p ${DIR}/app/tmp/cache/models
mkdir -p ${DIR}/app/tmp/cache/persistent
mkdir -p ${DIR}/app/tmp/cache/views

mkdir -p ${DIR}/app/tmp/log
mkdir -p ${DIR}/app/tmp/sessions
mkdir -p ${DIR}/app/tmp/tests

chown -R sisir:www-data ${DIR}
chmod -R g+w ${DIR}/app/tmp/


